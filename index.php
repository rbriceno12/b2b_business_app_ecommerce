<!DOCTYPE html>
<html lang="en">

<head>
    <title>TKotizo</title>
    <?php include('views/head.php'); ?>
</head>

<body>

    <!-- Topbar Start -->
    <?php include('views/topbar.php'); ?>
    <!-- Topbar End -->


    <!-- Navbar Start -->
    <?php include('views/navbar.php'); ?>
    <!-- Navbar End -->


    <!-- Featured Start -->
    <div class="container-fluid pt-5 pb-5" style="background: #00387e05 !important;">
        <div class="text-center" style="padding: 30px;">
            <h1 style="text-align: center;">Partners</h1>
            <span>Nuestros socios son los mejores distribuidores del pais</span>
        </div>
        <div class="row px-xl-5 pb-3 partners-row">
            
        </div>
    </div>
    <div class="container-fluid pt-5 pb-5">
        <div class="text-center" style="padding: 30px;">
            <h1 style="text-align: center;">Servicios</h1>
            <span>Cotiza tu proxima compra con nosotros</span>
        </div>
        
        <div class="row px-xl-5 pb-3">
            <div class="col-lg-3 col-md-6 col-sm-12 pb-1">
                <div class="d-flex align-items-center mb-4" style="padding: 30px; border: 1px solid #00387e !important; border-radius: 10px;">
                    <h1 class="fa fa-check text-primary m-0 mr-3"></h1>
                    <h5 class="font-weight-semi-bold m-0">Productos Respaldados</h5>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-12 pb-1">
                <div class="d-flex align-items-center mb-4" style="padding: 30px; border: 1px solid #00387e !important; border-radius: 10px;">
                    <h1 class="fa fa-shipping-fast text-primary m-0 mr-2"></h1>
                    <h5 class="font-weight-semi-bold m-0">Envio Gratis</h5>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-12 pb-1">
                <div class="d-flex align-items-center mb-4" style="padding: 30px; border: 1px solid #00387e !important; border-radius: 10px;">
                    <h1 class="fas fa-exchange-alt text-primary m-0 mr-3"></h1>
                    <h5 class="font-weight-semi-bold m-0">Entrega Rapida</h5>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-12 pb-1">
                <div class="d-flex align-items-center mb-4" style="padding: 30px; border: 1px solid #00387e !important; border-radius: 10px;">
                    <h1 class="fa fa-phone-volume text-primary m-0 mr-3"></h1>
                    <h5 class="font-weight-semi-bold m-0">Soporte las 24 horas</h5>
                </div>
            </div>
        </div>
    </div>
    <!-- Featured End -->

    
    <!-- Categories Start -->
    <div class="container-fluid pt-5 pb-5" style="background: #00387e05 !important;">
        <div class="text-center" style="padding: 30px;">
            <h1 style="text-align: center;">Categorias</h1>
            <span>Conoce algunos de los productos que puedes cotizar con nosotros</span>
        </div>
        <div class="row px-xl-5 pb-3 categories-row">
            
        </div>
    </div>
    <!-- Categories End -->

    <!-- Subscribe Start -->
    <div class="container-fluid bg-secondary my-5">
        <div class="row justify-content-md-center py-5 px-xl-5">
            <div class="col-md-6 col-12 py-5">
                <div class="text-center mb-2 pb-2">
                    <h2 class="section-title px-5 mb-3"><span class="bg-secondary px-2">Stay Updated</span></h2>
                    <p>Amet lorem at rebum amet dolores. Elitr lorem dolor sed amet diam labore at justo ipsum eirmod duo labore labore.</p>
                </div>
                <form action="">
                    <div class="input-group">
                        <input type="text" class="form-control border-white p-4" placeholder="Email Goes Here">
                        <div class="input-group-append">
                            <button style="color:white;" class="btn btn-primary px-4">Subscribe</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- Subscribe End -->

    <!-- Products Start -->
    <div class="container-fluid pt-5">
        <div class="text-center mb-4">
            <h2 class="section-title px-5"><span class="px-2">Productos Principales</span></h2>
        </div>
        <div class="row px-xl-5 pb-3 products-row">
            
        </div>
    </div>
    <!-- Products End -->


    

    <!-- Footer Start -->
    <?php include('views/footer.php') ?>
    <!-- Footer End -->


    <!-- Back to Top -->
    <!-- <script src="js/jquery/jquery.min.js"></script> -->

    <?php include('views/scripts.php') ?>
    <script>
        validateSession()
    </script>
    <script src="js/purchase-orders.js"></script>
    <script src="js/categories.js"></script>
    <script src="js/partners.js"></script>
    <script src="js/products.js"></script>

</body>

</html>
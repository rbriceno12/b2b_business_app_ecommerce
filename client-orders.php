<!DOCTYPE html>
<html lang="en">

<head>
    <title>TKotizo</title>
    <?php include('views/head.php'); ?>
</head>

<body>

    <!-- Topbar Start -->
    <?php include('views/topbar.php'); ?>
    <!-- Topbar End -->
    <!-- Navbar Start -->
    <?php include('views/navbar.php'); ?>
    <!-- Navbar End -->


   <div class="container-fluid pt-5">
    <table class="table">
        <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">Email</th>
                <th scope="col">File</th>
                <th scope="col">Total</th>
                <th scope="col">Comision</th>
                <th scope="col">Status</th>
                <th scope="col">Created</th>
            </tr>
        </thead>
        <tbody class="client-orders">
            
        </tbody>
    </table>
   </div>
    <!-- Vendor End -->


    <!-- Footer Start -->
    <?php include('views/footer.php') ?>
    <!-- Footer End -->


    <!-- Back to Top -->
    <a href="#" class="btn btn-primary back-to-top"><i class="fa fa-angle-double-up"></i></a>
    <!-- <script src="js/jquery/jquery.min.js"></script> -->

    <?php include('views/scripts.php') ?>
    <script>
        validateSession()
    </script>
    <script src="js/purchase-orders.js"></script>
    <script>
        listPurchaseOrders(localStorage.getItem('client_email'))
    </script>
    
    <script src="js/categories.js"></script>
</body>

</html>
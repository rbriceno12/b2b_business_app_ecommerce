
<!-- Modal -->
<div class="modal fade" id="loginModal" tabindex="-1" aria-labelledby="loginModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="loginModalLabel">Login</h5>
                
            </div>
            <div class="modal-body">
                <div class="login-message"></div>
                <div class="container">
                    <div class="row justify-content-center">
                        <div class="col-lg-10 col-md-10 d-flex flex-column align-items-center justify-content-center">
            
                            <div class="d-flex justify-content-center py-4">
                                <a href="index.html" class="logo d-flex align-items-center w-auto">
                                    <img src="assets/img/logo.png" alt="">
                                    <span class="d-none d-lg-block">TKotizo</span>
                                </a>
                            </div><!-- End Logo -->
            
                            <div class="card mb-3">
            
                                <div class="card-body">
                                    <div class="pt-4 pb-2">
                                        <div class="auth-message"></div>
                                    </div>
                                    <div class="pt-4 pb-2">
                                        <h5 class="card-title text-center pb-0 fs-4">Login to Your Account</h5>
                                        <p class="text-center small">Enter your username & password to login</p>
                                    </div>
                
                                    <form class="row g-3 needs-validation" novalidate>
                
                                        <div class="col-12">
                                            <label for="yourUsername" class="form-label">Email</label>
                                            <div class="input-group has-validation">
                                            <span class="input-group-text" id="inputGroupPrepend">@</span>
                                            <input type="email" name="email" class="form-control" id="login-email" required>
                                            <div class="invalid-feedback">Please enter your email.</div>
                                            </div>
                                        </div>
                    
                                        <div class="col-12">
                                            <label for="yourPassword" class="form-label">Password</label>
                                            <input type="password" name="password" class="form-control" id="login-password" required>
                                            <div class="invalid-feedback">Please enter your password!</div>
                                        </div>
                    
                                        <div class="col-12">
                                            <button id="login-button" class="btn btn-primary w-100" type="button" onclick="login()">Login</button>
                                        </div>
                                        <div class="col-12">
                                            <p class="small mb-0">No tienes una cuenta? <a href="#" onclick="$('#loginModal').modal('hide'); $('#registerModal').modal('show');">Registrarse</a></p>
                                        </div>
                                    </form>
                
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="registerModal" tabindex="-1" aria-labelledby="registerModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="registerModalLabel">Register</h5>
                
            </div>
            <div class="modal-body">
                <div class="register-message"></div>
                <div class="container">
                    <div class="row justify-content-center">
                        <div class="col-lg-10 col-md-10 d-flex flex-column align-items-center justify-content-center">
            
                            <div class="d-flex justify-content-center py-4">
                                <a href="index.html" class="logo d-flex align-items-center w-auto">
                                    <img src="assets/img/logo.png" alt="">
                                    <span class="d-none d-lg-block">TKotizo</span>
                                </a>
                            </div><!-- End Logo -->
            
                            <div class="card mb-3">
            
                                <div class="card-body">
                                    <div class="pt-4 pb-2">
                                        <div class="auth-message"></div>
                                    </div>
                                    <div class="pt-4 pb-2">
                                        <h5 class="card-title text-center pb-0 fs-4">Register to Your Account</h5>
                                        <p class="text-center small">Enter your name, email & password to register</p>
                                    </div>
                
                                    <form class="row g-3 needs-validation" novalidate>

                                        <div class="col-12">
                                            <label for="yourName" class="form-label">Email</label>
                                            <div class="input-group has-validation">
                                            <span class="input-group-text" id="inputGroupPrepend">@</span>
                                            <input type="text" name="name" class="form-control" id="register-name" required>
                                            <div class="invalid-feedback">Please enter your name.</div>
                                            </div>
                                        </div>
                                        <div class="col-12">
                                            <label for="yourUsername" class="form-label">Email</label>
                                            <div class="input-group has-validation">
                                            <span class="input-group-text" id="inputGroupPrepend">@</span>
                                            <input type="email" name="email" class="form-control" id="register-email" required>
                                            <div class="invalid-feedback">Please enter your email.</div>
                                            </div>
                                        </div>
                    
                                        <div class="col-12">
                                            <label for="yourPassword" class="form-label">Password</label>
                                            <input type="password" name="password" class="form-control" id="register-password" required>
                                            <div class="invalid-feedback">Please enter your password!</div>
                                        </div>
                    
                                        <div class="col-12">
                                            <button id="register-button" class="btn btn-primary w-100" type="button" onclick="register()">Register</button>
                                        </div>
                                        <div class="col-12">
                                            <p class="small mb-0">Ya tienes una cuenta? <a href="#" onclick="$('#registerModal').modal('hide'); $('#loginModal').modal('show');">Login</a></p>
                                        </div>
                                    </form>
                
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="createPurchaseOrderModal" tabindex="-1" aria-labelledby="createPurchaseOrderModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="createPurchaseOrderModalLabel">Selecciona el archivo de tu cotizacion</h5>
          
        </div>
        <div class="modal-body">
          <div class="create-purchase-order-message"></div>
          <div class="container">
            <form id="createPurchaseOrderForm">
                <div class="row mb-3">
                    <label for="inputNumber" class="col-sm-2 col-form-label">Selecciona un archivo:</label>
                    <div class="col-sm-10">
                        <input class="form-control" type="file" id="file" name="file">
                    </div>
                </div>
                <div class="row mb-3 new-client">
                    <input class="form-control" type="text" id="client-email" name="email" readonly>
                </div>
                
                <!-- <select class="form-select" multiple aria-label="multiple select example">
                    <option selected>Open this select menu</option>
                    <option value="1">One</option>
                    <option value="2">Two</option>
                    <option value="3">Three</option>
                </select> -->
                <button id="create-purchase-order-button" type="submit" class="btn btn-primary">Guardar</button>
            </form>
          </div>
          
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
          <!-- <button id="create-user-button" onclick="createPurchaseOrder()" type="button" class="btn btn-primary">Save changes</button> -->
        </div>
      </div>
    </div>
</div>

<div class="container-fluid">
    <div class="row bg-secondary py-2 px-xl-5">
        <div class="col-lg-6 d-none d-lg-block">
            <div class="d-inline-flex align-items-center">
                <a class="text-dark" href="">FAQs</a>
                <span class="text-muted px-2">|</span>
                <a class="text-dark" href="">Help</a>
                <span class="text-muted px-2">|</span>
                <a class="text-dark" href="">Support</a>
            </div>
        </div>
        <div class="col-lg-6 text-center text-lg-right">
            <div class="d-inline-flex align-items-center">
                <a class="text-dark px-2" href="">
                    <i class="fab fa-facebook-f"></i>
                </a>
                <a class="text-dark px-2" href="">
                    <i class="fab fa-twitter"></i>
                </a>
                <a class="text-dark px-2" href="">
                    <i class="fab fa-linkedin-in"></i>
                </a>
                <a class="text-dark px-2" href="">
                    <i class="fab fa-instagram"></i>
                </a>
                <a class="text-dark pl-2" href="">
                    <i class="fab fa-youtube"></i>
                </a>
            </div>
        </div>
    </div>
    <div class="row align-items-center py-3 px-xl-5">
        <div class="col-lg-3 d-none d-lg-block">
            <a href="" class="text-decoration-none">
                <img src="img/logoazul.png" alt="">
                <!-- <h1 class="m-0 display-5 font-weight-semi-bold"><span class="text-primary font-weight-bold border px-3 mr-1">E</span>Tkotizo</h1> -->
            </a>
        </div>
        <div class="col-lg-6 col-6 text-left">
            <!-- <form action="">
                <div class="input-group">
                    <input type="text" class="form-control" placeholder="Search for products">
                    <div class="input-group-append">
                        <span class="input-group-text bg-transparent text-primary">
                            <i class="fa fa-search"></i>
                        </span>
                    </div>
                </div>
            </form> -->
        </div>
        <div class="col-lg-3 col-6 text-right show-loggedin-buttons" style="display: none;">
            <a type="button" class="btn btn-success" data-toggle="modal" data-target="#createPurchaseOrderModal" href="#">
                <i class="fas fa-plus text-primary"></i>&nbsp;&nbsp;
                <!-- <span class="badge">0</span> -->
                 Cotizar
            </a>
            <a type="button" class="btn btn-secondary" data-toggle="tooltip" data-placement="top" title="Mis Compras" href="client-orders.php">
                <i class="fas fa-list text-primary"></i>
                <span class="badge"></span>
            </a>
            
        </div>
    </div>
</div>
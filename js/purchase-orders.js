$(document).ready(function (){

    $('#client-email').val(localStorage.getItem('client_email'))

    $('#createPurchaseOrderForm').submit(function(e) {
        e.preventDefault(); // Prevent default form submission
        const file = $('#file').val(),
              email = $('#client-email').val(),
        createPurchaseOrderButton = $('#create-purchase-order-button')


        if(!file){
            $('.create-purchase-order-message').html(
        
                `<div class="alert alert-danger alert-dismissible fade show" role="alert">
                    Upload a file
                    <button type="button" class="btn-close" data-dismiss="alert" aria-label="Close"></button>
                </div>`
            )
        }else{
            createPurchaseOrderButton.html(
                `<div class="spinner-border text-light" role="status">
                    <span class="visually-hidden"></span>
                </div>`
            )
            const formData = new FormData(this)

            $.ajax({
                url: 'http://34.222.146.56:8000/purchase-orders/clients/', // URL of your Node.js route handler
                type: 'POST',
                data: formData, // Attach FormData object to request
                contentType: false, // Set content type to false for multipart/form-data
                processData: false, // Prevent jQuery from processing data
                success: (data) => {
                    $('.create-purchase-order-message').html(
        
                        `<div class="alert alert-success alert-dismissible fade show" role="alert">
                            Su orden de compra ha sido procesada! En Breve nos comunicaremos con ustedes.
                            <button type="button" class="btn-close" data-dismiss="alert" aria-label="Close"></button>
                        </div>`
                    )
                    createPurchaseOrderButton.prop('disabled', false)
                    createPurchaseOrderButton.html(
                        `Save changes`
                    )
                    $('#client-name').val('')
                    $('#file').val('')
                    location.href="client-orders.php"
                    // Handle successful upload response
                },
                error: (jqXHR, textStatus, errorThrown) => {
                    console.error('Upload failed:', textStatus, errorThrown);
                    // Handle upload error
                }
            });
        }
    
    });
})


const listPurchaseOrders = (email) => {


    $.ajax({
        method: 'POST',
        // url: 'http://34.222.146.56:8000/purchase-orders/list/client/',
        url: 'http://34.222.146.56:8000/purchase-orders/list/client/',
        data: JSON.stringify({email:email}),
        headers: {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + localStorage.getItem('client_token')
        },
        success: (res) => {

            console.log(res)

            let listBody = ``
            let showButtons
            let greenBackground
            let counter = 0
            let showConfirmButton = 'show-button'
            let showDeleteButton = 'show-button'
            let showDesactivateButton = 'show-button'

            res.data.forEach(purchaseOrder => {

                let status = ''
                
                switch (purchaseOrder.status) {
                    case 'purchase-order':

                        showConfirmButton = ''
                        showDeleteButton = ''
                        showDesactivateButton = ''
                        status = 'En Proceso'

                        break;
                    case 'cancelled-purchase-order':

                        showConfirmButton = ''
                        showDeleteButton = ''
                        showDesactivateButton = ''
                        status = 'Cancelada'
                        break;
                    case 'invoice':

                        showConfirmButton = ''
                        showDeleteButton = ''
                        showDesactivateButton = ''
                        status = 'Procesada'

                        break;
                    default:
                        break;
                }

                listBody+= `<tr>
                                <td>${purchaseOrder.id}</td>
                                <td>${purchaseOrder.email}</td>
                                <td> <a target="_blank" href="http://34.222.146.56:8000/public/uploads/${purchaseOrder.file}">Ver archivo</a></td>
                                <td>$${purchaseOrder.value}</td>
                                <td>$${purchaseOrder.comission ? purchaseOrder.comission : 0.00}</td>
                                <td>${status}</td>
                                <td>${purchaseOrder.created_at}</td>
                            </tr>`
                
            });

            $('.client-orders').html(
                listBody
            )
            
        },
        statusCode: {
            401: () => {
                console.log('error')
                // localStorage.clear()
                // location.href="./"
            },
            403: () => {
                console.log('error')
                // localStorage.clear()
                // location.href="./"
            },
            500: () => {
                console.log('error')
            }
        }
    })
}

const login = () => {

    const email = $('#login-email').val(),
          password = $('#login-password').val(),
          loginButton = $('#login-button')

    if(!email || !password){
        $('.login-message').html(

            `<div class="alert alert-danger alert-dismissible fade show" role="alert">
                Ingrese un email y un password
            </div>`
        )
    }else{
        loginButton.html(
            `<div class="spinner-border text-light" role="status">
                <span class="visually-hidden"></span>
            </div>`
        )
    
        loginButton.prop('disabled', true)
    
        $.ajax({
            method: 'POST',
            url: 'http://34.222.146.56:8000/auth/login-users/',
            data: JSON.stringify({email: email, password: password}),
            headers: {
                'Content-Type': 'application/json'
            },
            success: (res) => {

                localStorage.setItem('client_token', res.token)
                localStorage.setItem('client_role_id', res.role_id)
                localStorage.setItem('client_email', email)
    
                $('.login-message').html(
    
                    `<div class="alert alert-success alert-dismissible fade show" role="alert">
                        Welcome back!
                        <button type="button" class="btn-close" data-dismiss="alert" aria-label="Close"></button>
                    </div>`
                )
                
                setTimeout(() => {
                    location.href = './'
                }, 2000);
            },
            statusCode: {
                401: () => {
                    $('.login-message').html(
    
                        `<div class="alert alert-danger alert-dismissible fade show" role="alert">
                            Wrong email or password, try again.
                            <button type="button" class="btn-close" data-dismiss="alert" aria-label="Close"></button>
                        </div>`
                    )
                    loginButton.prop('disabled', false)
                    loginButton.html(
                        `Login`
                    )
                },
                500: () => {
                    $('.login-message').html(
    
                        `<div class="alert alert-danger alert-dismissible fade show" role="alert">
                            We have a problem processing your request, try again later.
                            <button type="button" class="btn-close" data-dismiss="alert" aria-label="Close"></button>
                        </div>`
                    )
                    loginButton.prop('disabled', false)
                    loginButton.html(
                        `Login`
                    )
                }
            }
        })
    }
    
}

const register = () => {

    const name = $('#register-name').val(),
          email = $('#register-email').val(),
          password = $('#register-password').val(),
          registerButton = $('#register-button')

    if(!name || !email || !password){
        $('.register-message').html(

            `<div class="alert alert-danger alert-dismissible fade show" role="alert">
                Ingrese un email y un password
            </div>`
        )
    }else{
        registerButton.html(
            `<div class="spinner-border text-light" role="status">
                <span class="visually-hidden"></span>
            </div>`
        )
    
        registerButton.prop('disabled', true)
    
        $.ajax({
            method: 'POST',
            url: 'http://34.222.146.56:8000/auth/register-users/',
            data: JSON.stringify({name: name, email: email, password: password}),
            headers: {
                'Content-Type': 'application/json'
            },
            success: (res) => {

                localStorage.setItem('client_token', res.token)
                localStorage.setItem('client_role_id', res.role_id)
                localStorage.setItem('client_email', email)
    
                $('.register-message').html(
    
                    `<div class="alert alert-success alert-dismissible fade show" role="alert">
                        Welcome!
                        <button type="button" class="btn-close" data-dismiss="alert" aria-label="Close"></button>
                    </div>`
                )
                
                setTimeout(() => {
                    location.href = './'
                }, 2000);
            },
            statusCode: {
                401: () => {
                    $('.register-message').html(
    
                        `<div class="alert alert-danger alert-dismissible fade show" role="alert">
                            Wrong email or password, try again.
                            <button type="button" class="btn-close" data-dismiss="alert" aria-label="Close"></button>
                        </div>`
                    )
                    registerButton.prop('disabled', false)
                    registerButton.html(
                        `Register`
                    )
                },
                500: () => {
                    $('.register-message').html(
    
                        `<div class="alert alert-danger alert-dismissible fade show" role="alert">
                            We have a problem processing your request, try again later.
                            <button type="button" class="btn-close" data-dismiss="alert" aria-label="Close"></button>
                        </div>`
                    )
                    registerButton.prop('disabled', false)
                    registerButton.html(
                        `Register`
                    )
                }
            }
        })
    }
    
}

const validateToken = (p) => {

    console.log('Entramos a la funcion')

    $.ajax({
        method: 'GET',
        url: `http://34.222.146.56:8000/auth/verify/`,
        headers: {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + localStorage.getItem('client_token')
        },
        success: (res) => {

            if(p == true){
                $('.show-loggedin-buttons').fadeIn('fast')
                $('.show-create-purchase-order').fadeIn('fast')
                $('.auth-menu').html(`<a href="#" class="nav-item nav-link">${localStorage.getItem('client_email')} <i class="fas fa-user text-primary"></i></a>
                <a href="#" onclick="logout()" class="nav-item nav-link">Logout <i class="fas fa-arrow-right text-primary"></i></a>`)
            }
            
            return true
        },
        statusCode: {
            403: () => {
                if(p == true){

                    $('.auth-menu').html(`<a href="#" class="nav-item nav-link" data-toggle="modal" data-target="#loginModal">Login</a>
                    <a href="#" data-toggle="modal" data-target="#registerModal" class="nav-item nav-link">Register</a>`)
                    $('.show-loggedin-buttons').fadeOut('fast')
                    $('.show-create-purchase-order').fadeOut('fast')

                }
                
                return false
            },
            500: () => {
                if(p == true){

                    $('.auth-menu').html(`<a href="#" class="nav-item nav-link" data-toggle="modal" data-target="#loginModal">Login</a>
                    <a href="#" data-toggle="modal" data-target="#registerModal" class="nav-item nav-link">Register</a>`)
                    $('.show-loggedin-buttons').fadeOut('fast')
                    $('.show-create-purchase-order').fadeOut('fast')

                }
                return false
            }
        }
    })
}

validateToken(true)

// const validateLoggedUser = () => {
//     console.log(validateToken(false))
//     // validateToken(false) == true ? $('#createPurchaseOrderModal').modal('show') : $('#loginModal').modal('show')

// }

const validateSession = () => {

    // if(localStorage.getItem('client_token') === null){
    //     console.log('No session')
    //     $('.auth-menu').html(`<a href="#" class="nav-item nav-link" data-toggle="modal" data-bs-target="#loginModal">Login</a>
    //     <a href="#" class="nav-item nav-link">Register</a>`)
    //     $('.show-loggedin-buttons').fadeOut('fast')
    // }else{
    //     console.log('Si session')
    //     $('.show-loggedin-buttons').fadeIn('fast')
    //     $('.auth-menu').html(`<a href="#" class="nav-item nav-link">${localStorage.getItem('client_email')} <i class="fas fa-user text-primary"></i></a>
    //     <a href="#" onclick="logout()" class="nav-item nav-link">Logout <i class="fas fa-arrow-right text-primary"></i></a>`)
    // }
}


const logout = () => {
    localStorage.clear()
    validateToken(false) == true ? '' : location.href="./"
}
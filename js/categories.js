const listCategories = (created) => {


    $.ajax({
        method: 'GET',
        url: 'http://34.222.146.56:8000/categories/list/',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + localStorage.getItem('client_token')
        },
        success: (res) => {

            let listBody = ``,
                rowBody = ``,
                dropDownBody = ``

            res.data.forEach(category => {
                    listBody+= `
                        <a href="#" class="nav-item nav-link">${category.name}</a>
                    `
                    rowBody+= `
                        <div class="col-lg-3 col-md-4 pb-1">
                            <div class="cat-item d-flex flex-column mb-4" style="padding: 30px; border: 1px solid #00387e !important; border-radius: 10px;">
                                <a style="text-align: center;" href="category.php?${category.name}" class="cat-img position-relative overflow-hidden mb-3">
                                    <img style="width: 400px; height: 320px;" class="img-fluid" src="http://34.222.146.56:8000/public/uploads/${category.img}" alt="">
                                </a>
                                <h5 style="text-align: center;" class="font-weight-semi-bold m-0">${category.name}</h5>
                            </div>
                        </div>
                    `
                    dropDownBody+= `
                        <a href="categories.php?${category.id}" class="dropdown-item">${category.name}</a>
                    `
            });

            $('.categories-nav').html(listBody)
            $('.categories-row').html(rowBody)
            // $('.categories-dropdown').html(dropDownBody)
            
        },
        statusCode: {
            401: () => {
                console.log('error')
            },
            500: () => {
                console.log('error')
            }
        }
    })
}

listCategories(false)

const listPartners = () => {


    $.ajax({
        method: 'GET',
        url: 'http://34.222.146.56:8000/partners/public',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + localStorage.getItem('client_token')
        },
        success: (res) => {

            let listBody = ``

            res.data.forEach(partner => {
                    listBody+= `
                        <div class="col-lg-3 col-md-4 pb-1">
                            <div class="cat-item d-flex flex-column mb-4" style="padding: 30px; border: 1px solid #00387e !important; border-radius: 10px;">
                                <a style="text-align: center;" href="partner.php?${partner.name}" class="cat-img position-relative overflow-hidden mb-3">
                                    <img style="width: 200px; height: 160px;" class="img-fluid" src="http://34.222.146.56:8000/public/uploads/${partner.img}" alt="">
                                </a>
                                <h5 style="text-align: center;" class="font-weight-semi-bold m-0">${partner.name}</h5>
                            </div>
                        </div>
                    `
            });

            $('.partners-row').html(listBody)
            
        },
        statusCode: {
            401: () => {
                console.log('error')
            },
            500: () => {
                console.log('error')
            }
        }
    })
}

listPartners()
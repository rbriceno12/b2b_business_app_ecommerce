// `<a style="text-align: center;" href="product.php?${product.name}" class="cat-img position-relative overflow-hidden mb-3">
//                                     <img style="width: 200px; height: 160px;" class="img-fluid" src="http://34.222.146.56:8000/public/uploads/${product.img}" alt="">
//                                 </a>`



const listProducts = () => {


    $.ajax({
        method: 'GET',
        url: 'http://34.222.146.56:8000/products/public/',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + localStorage.getItem('client_token')
        },
        success: (res) => {

            let listBody = ``

            res.data.forEach(product => {
                    listBody+= `<div class="col-lg-3 col-md-6 col-sm-12 pb-1">
                                    <div class="card product-item border-0 mb-4" style="width: 500px; height: 500px;">
                                        <div class="card-header product-img position-relative overflow-hidden bg-transparent border p-0">
                                            <img class="img-fluid w-100" src="http://34.222.146.56:8000/public/uploads/${product.img}" alt="">
                                        </div>
                                        <div class="card-body border-left border-right text-center p-0 pt-4 pb-3">
                                            <h6 class="text-truncate mb-3">${product.name}</h6>
                                        </div>
                                        <div class="card-footer d-flex justify-content-between bg-light border">
                                            <a href="#" class="btn btn-sm text-dark p-0"><i class="fas fa-eye text-primary mr-1"></i>Vendido por ${product.partner}</a>
                                        </div>
                                    </div>
                                </div>`
            });

            $('.products-row').html(listBody)
            
        },
        statusCode: {
            401: () => {
                console.log('error')
            },
            500: () => {
                console.log('error')
            }
        }
    })
}

listProducts()